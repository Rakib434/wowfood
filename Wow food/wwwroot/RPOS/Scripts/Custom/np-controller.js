﻿var npGlobalProp = {
    npPropSplitter: '^',
    npObjSplitter: '~',
    npObjNameSplitter: '<prop>',
    npListSplitter: '<list>',
    base_url: window.location.origin
};
var nptable = {
    npTrOpen: '<tr>',
    npTrClose: '</tr>',
    npTdOpen: '<td>',
    npTdClose: '</td>',
    npTheadOpen: '<thead>',
    npTheadClose: '</thead>',
    npTbodyOpen: '<tbody>',
    npTbodyClose: '</tbody>',
    npTableOpen: '<table>',
    npTableClose: '</table>',
};
var toggleCheck = function (control, chkctrl) {
    var checkAll = $("#" + control).prop('checked');
    
    if (checkAll) {
        $("." + chkctrl).prop("checked", true);
    } else {
        $("." + chkctrl).prop("checked", false);
    }
};
var customtoggleCheck = function () {
    
};

var copyOnChange = function (ctrl, affectedCtrl) {
    console.log(ctrl);
    console.log(affectedCtrl);
    var ctrls = affectedCtrl.split('^');
    $(ctrls).each(function () {
        $('#' + this).val(ctrl.value);
    });
};
//$("#checkAll").on("change", function () {
//    var container = $(this).parent().parent().next();
//    if ($(this).is(":checked")) {
//        $(container).find("input:checkbox").each(function () {
//            $(this).prop('checked', true);
//        });
//    } else {
//        $(container).find("input:checkbox").each(function () {
//            $(this).prop('checked', false);
//        });
//    }
//});

var npAlert = function (msg) {
    if (msg.MessageType == 1) {
        alertify.success(msg.ReturnValue);
    } else if (msg.MessageType == 2) {
        alertify.error(msg.ReturnValue);
    } else if (msg.MessageType == 3) {
        alertify.error(msg.ReturnValue);
    } else if (msg.MessageType == 4) {
        alertify.error(msg.ReturnValue);
    } else if (msg.MessageType == 5) {
        alertify.error(msg.ReturnValue);
    }
};
var npNotification = function (msg) {
    if (msg.ReturnValue == null || typeof msg.ReturnValue === 'object') msg.ReturnValue = "";
    $('.addclass').removeClass('alert');
    $('.addclass').removeAttr("style");
    //console.log($('.addclass').html());
    $('.addclass').addClass('alert');
    if (msg.MessageType == 1) {
        $('.alert').addClass('alert-success');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $('.alert').fadeIn('slow');
        $(".alert").delay(3000).fadeOut("slow");
    } else if (msg.MessageType == 2) {
        $('.alert').addClass('alert-danger');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $(".alert").delay(3000).fadeOut("slow");
    } else if (msg.MessageType == 3) {
        $('.alert').addClass('alert-warning');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $(".alert").delay(3000).fadeOut("slow");
    } else if (msg.MessageType == 4) {
        $('.alert').addClass('alert-warning');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $(".alert").delay(3000).fadeOut("slow");
    } else if (msg.MessageType == 5) {
        $('.alert').addClass('alert-warning');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $(".alert").delay(3000).fadeOut("slow");
    } else if (msg.MessageType == 6) {
        $('.alert').addClass('alert-danger');
        $('.alert').html(msg.MessageData + '\n' + msg.ReturnValue);
        $(".alert").delay(3000).fadeOut("slow");
    }
};
/* this function intend to save ready data to server.
   **  pdata object create dynamically. it could be single object, list of object or 
       an object which contain both.
   **  url contain controller -> method name.
   **  redirectUrl contain url name if system needs to redirect to another method.
*/
// this function prepare options of select input box from javascript list object.
var GenerateDropDownList = function (listObj, control, requiredField) {
    $('select#' + control).empty();
    var pOptionValues = '';
    requiredField = requiredField.split(window.npGlobalProp.npPropSplitter);
    pOptionValues += '<option value="0">Select One</option>';
    $.each(listObj, function (key, value) {
        pOptionValues += '<option value="' + value[requiredField[0]] + '">' + value[requiredField[1]] + '</option>';
    });
    $('select#' + control).append(pOptionValues);
};


var createControl = function (ctrltype) {
    var spctrlnid = ctrltype.split('_');
    //console.log(ctrltype);
    if (spctrlnid[0] == 'textbox') {
        var onkeyuparr = spctrlnid[1].split(window.npGlobalProp.npObjSplitter);
        /// 1 means textbox with onkeyup function
        /// 2 means textbox with normal
        /// 3 means textbox with readonly
        if (onkeyuparr[0] == 1) {
            //console.log(onkeyuparr[1]);
            var controls = onkeyuparr[1].split('$');
            //console.log(controls);
            return '<input type="text" id="' + controls[0] + '" class="form-control ' + controls[0] + '" value="0" onkeyup="' + controls[1] + '" />';
        } else if (onkeyuparr[0] == 3) {
            //console.log(onkeyuparr[1]);
            //console.log(controls);
            return '<input type="text" id="' + onkeyuparr[1] + '" class="form-control ' + onkeyuparr[1] + '" value="0" readonly="readonly" />';
        } else {
            return '<input type="text" id="' + onkeyuparr[1] + '" class="form-control ' + onkeyuparr[1] + '" value="0" />';
        }
    }
    if (spctrlnid[0] == 'btn') {
        return '<button class="btn btn-default" onclick="' + spctrlnid[1] + '">Remove</button>';
    }
    return '';
};

var GenerateTableRow = function (addrowprop) {
    //console.log("Generating Table Row.");
    if ($('#' + addrowprop.control).val() == '' || $('#' + addrowprop.control).val() == null) return false;
    $.ajax({
        type: 'POST',
        url: addrowprop.url,
        data: { 'param': $('#' + addrowprop.control).val() },
        cache: true,
        crossDomain: true,
        success: function (result) {
            var trbody = nptable.npTrOpen;
            if (result.MessageType != 1) {
                alertify.error(result.MessageData);
                return false;
            }
            var isExist = false;
            result = result.ReturnValue;
            console.log(result);
            $('#' + addrowprop.appendTo + ' tr').find('td:first').each(function () {
                if (!isExist) {
                    var firstelementval = $(this).html();
                    if (result.hasOwnProperty(addrowprop.primaryIndex)) {
                        if (result[addrowprop.primaryIndex] == firstelementval) {
                            alertify.error("Data duplicate.");
                            isExist = true;
                        }
                    }
                }
            });
            if (!isExist) {
                $.each(addrowprop.showableObjects.split(window.npGlobalProp.npPropSplitter), function (key, value) {
                    if (result.hasOwnProperty(value)) {
                        if (addrowprop.hiddenObjects.indexOf(value) > -1) {
                            trbody += '<td class="' + value + ' hidden" np-key="' + value + '">' + result[value] + nptable.npTdClose;
                        } else {
                            trbody += '<td class="' + value + ' " np-key="' + value + '">' + result[value] + nptable.npTdClose;
                        }
                    } else {
                        trbody += '<td class="' + value.split('_')[0] + '"  np-key="' + value.split('_')[0] + '">' + createControl(value) + nptable.npTdClose;
                    }
                });
                trbody += nptable.npTrClose;
                $('#' + addrowprop.appendTo).append(trbody);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
    return true;
};
//this function prepare options of select input box from ajax call.

// clear the form value
var ClearForm = function (controls) {
    $.each(controls, function (key, objVal) {
        var selectedDiv = '.' + key;
        //console.log(selectedDiv);
        $.each(objVal, function (k, control) {
            control = '#' + control;
            if ($(selectedDiv).find(control).is('select') || $(selectedDiv).find(control).is('Select') || $(selectedDiv).find(control).is('SELECT')) {
                $(selectedDiv).find(control + ' option:first').attr('selected', 'selected');
            }
            if ($(selectedDiv).find(control).is('input:radio')) {
                $(selectedDiv).find(control).prop('checked', false);
            }
            if ($(selectedDiv).find(control).is('input:text')) {
                $(selectedDiv).find(control).val('');
            }
            if ($(selectedDiv).find(control).is('textarea')) {
                $(selectedDiv).find(control).val('');
            }
            if ($(selectedDiv).find(control).is('input:password')) {
                $(selectedDiv).find(control).val('');
            }
            if ($(selectedDiv).find(control).is('input:hidden')) {
                $(selectedDiv).find(control).val('');
            }
            if ($(selectedDiv).find(control).is('input:checkbox')) {
                $(selectedDiv).find(control).attr('checked', false);
            }
        });
    });
};
// clear the form value ends here
// prepare full form from object
var SetObjects = function (objects) {
    //console.log(objects);
    $.each(objects, function (key, objVal) {
        var selectedDiv = '.' + key;
        if (Object.prototype.toString.call(objVal) === '[object Array]') {
            objVal = objVal[0];
        }
        $.each(objVal, function (k, value) {
            var control = '#' + k;
            if ($(selectedDiv).find(control).is('select') || $(selectedDiv).find(control).is('Select') || $(selectedDiv).find(control).is('SELECT')) {
                $('select' + control + ' option').each(function () {
                    if ($(this).val() == value) {
                        $(this).attr("selected", "selected");
                    }
                });
            }
            if ($(selectedDiv).find(control).is('input:radio')) {
                //$(selectedDiv).find(control).prop('checked', false);
            }
            if ($(selectedDiv).find(control).is('input:text')) {
                if (typeof (value) == "string") {
                    $(selectedDiv).find(control).val(ConvertMillisecondToDate(value));
                } else {
                    $(selectedDiv).find(control).val(value);
                }
            }
            if ($(selectedDiv).find(control).is('textarea')) {
                $(selectedDiv).find(control).val(value);
            }
            if ($(selectedDiv).find(control).is('input:password')) {
                $(selectedDiv).find(control).val(value);
            }
            if ($(selectedDiv).find(control).is('input:hidden')) {
                $(selectedDiv).find(control).val(value);
            }
            if ($(selectedDiv).find(control).is('input:checkbox')) {
                // $(selectedDiv).find(control).attr('checked', false);
            }
        });
    });
};
// prepare full form from object ends here
//return the value of html input field
//it will find the type and then find the value of that field.
var npGetPropVal = function (selectedDiv, control) {
    //console.log(selectedDiv);
    //console.log(control);
    if (control.indexOf('__R') > -1) {
        var res = control.replace("__R", "");
        return $('input[name="' + res + '"]:checked').val();
    }

    control = '#' + control;
    selectedDiv = '.' + selectedDiv;
    if (control.indexOf('__D') > -1) {
        var resarr = control.split("__D");
        var datevalue = $(selectedDiv).find(resarr[0]).val();
        console.log(datevalue);
        var datetype = resarr[1];
        if (datetype == 'ddmmyyyy' || datetype == 'ddmmmyyyy')
            return datevalue.split('/')[1] + '/' + datevalue.split('/')[0] + '/' + datevalue.split('/')[2];
    }
    if ($(selectedDiv).find(control).is('select') || $(selectedDiv).find(control).is('Select') || $(selectedDiv).find(control).is('SELECT')) {
        return $(selectedDiv).find(control).find(':selected').attr('value');
    }
    if ($(selectedDiv).find(control).is('input:radio')) {
        return $(selectedDiv).find(control).attr('value');
    }
    if ($(selectedDiv).find(control).is('input:text')) {
        return $(selectedDiv).find(control).val();
    }
    if ($(selectedDiv).find(control).is('textarea')) {
        return $(selectedDiv).find(control).val();
    }
    if ($(selectedDiv).find(control).is('input:password')) {
        return $(selectedDiv).find(control).val();
    }
    if ($(selectedDiv).find(control).is('input:hidden')) {
        return $(selectedDiv).find(control).val();
    }
    if ($(selectedDiv).find(control).is('input:checkbox')) {
        if ($(selectedDiv).find(control).is(':checked')) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return " ";
    }
};
/*
example :
    **  string format 
    **  controlStr='<obj-name>table-name<param-list>col1^col2^col3'
    **  seperate all table first by split the line using objectSplitter
    after 
*/

var npGetObject = function (npObjName, npObjProp) {
    // npObjProp = col1^col2^col3
    var npTemp = {};
    var propList = npObjProp.split(window.npGlobalProp.npPropSplitter);
    // propList = ['col1','col2','col3'];
    propList.forEach(function (val) {
        // check if control is radio button or not.
        var tempVal = val;
        if (val.indexOf('__R') > -1) {
            tempVal = val.replace("__R", "");
        }
        if (val.indexOf('__D') > -1) {
            tempVal = val.split("__D")[0];
        }
        npTemp[tempVal] = window.npGetPropVal(npObjName, val);
    });
    // npTemp = {'col1':col1_value,'col2':col2_value,'col3:col3_value'};
    return npTemp;
};

var npGetList = function (npObjName, npObjProp) {
    // npObjProp = col1^col2^col3
    // npObjName = should be table name
    var npTempArr = [];
    var propList = npObjProp.split(window.npGlobalProp.npPropSplitter);
    var container = $('.' + npObjName);
    $.each(container, function () {
        $(this).find('tr').each(function () {
            var npTemp = {};
            $(this).find('td').each(function () {
                var key = $(this).attr('np-key');
                if (_.indexOf(propList, key) > -1) {
                    npTemp[key] = $(this).html();
                }
                if (key == 'textbox') {
                    $(this).find('input:text').each(function () {
                        var clname = $(this).attr('class').split(' ')[1];
                        if (_.indexOf(propList, clname) > -1) {
                            npTemp[clname] = $(this).val();
                        }
                    });
                }
            });
            npTempArr.push(npTemp);
        });
    });
    // npTempArr = {[0] = {'col1':col1_value,'col2':col2_value,'col3:col3_value'},
    //              [1] = {'col1':col1_value,'col2':col2_value,'col3:col3_value'}
    //              [2] = {'col1':col1_value,'col2':col2_value,'col3:col3_value'}};
    return npTempArr;
};

var npGetMegaObj = function (npFormControl) {
    var npMegaObj = {};
    var npObjName = '';
    var npObjProp = '';
    if (npFormControl.IsSingle) {

        npObjName = npFormControl.ControlStr.split(window.npGlobalProp.npObjNameSplitter)[0];
        // npObjName = tablename or <list>tablename
        npObjProp = npFormControl.ControlStr.split(window.npGlobalProp.npObjNameSplitter)[1];
        // npObjProp = col1^col2^col3

        if (npObjName.indexOf(window.npGlobalProp.npListSplitter) > -1) {
            // if npObjName == <list>tablename
            //objStr = <list>tablename<param-list>col1^col2^col3'
            npObjName = npObjName.split(window.npGlobalProp.npListSplitter)[1];
            npMegaObj[npObjName] = window.npGetList(npObjName, npObjProp);
        } else {
            // if npObjName == tablename
            //objStr = tablename<param-list>col1^col2^col3
            npMegaObj[npObjName] = window.npGetObject(npObjName, npObjProp);
        }
    } else {
        // split object by object splitter "~"
        var npObjContainer = npFormControl.ControlStr.split(window.npGlobalProp.npObjSplitter);
        // after split npObjContainer = [[0]='tablename<prop>col1^col2^col3',[1]='tablename<prop>col1^col2^col3',
        //                               [2]='<list>tablename<prop>col1^col2^col3'];
        npObjContainer.forEach(function (objStr) {
            npObjName = objStr.split(window.npGlobalProp.npObjNameSplitter)[0];
            // npObjName = tablename or <list>tablename
            npObjProp = objStr.split(window.npGlobalProp.npObjNameSplitter)[1];
            // npObjProp = col1^col2^col3
            if (npObjName.indexOf(window.npGlobalProp.npListSplitter) > -1) {
                // if npObjName == <list>tablename
                //objStr = <list>tablename<param-list>col1^col2^col3'
                npObjName = npObjName.split(window.npGlobalProp.npListSplitter)[1];
                npMegaObj[npObjName] = window.npGetList(npObjName, npObjProp);
            } else {
                // if npObjName == tablename
                //objStr = tablename<param-list>col1^col2^col3
                npMegaObj[npObjName] = window.npGetObject(npObjName, npObjProp);
            }
        });
    }
    return npMegaObj;
};

var JumpToUrl = function (redirectUrl) {
    location.href = redirectUrl;
};

var npSave = function (pdata, url, messageType) {
    var d = JSON.stringify(pdata);
    console.log(d);
    return $.ajax({
        type: 'POST',
        url: url,
        data: d,
        //dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        cache: false,
        crossDomain: true,
        success: function (result) {
            console.log(result);
            if (messageType == 'alert') {
                npAlert(result);
            } else {
                npNotification(result);
            }
            return result;
        },
        error: function (err) {
            //console.log(err);
            return false;
        }
    });
};

var npValidation = function (npMegaObj, npValidate) {
    var validationStatus = true;
    var validateArr = npValidate.validateStr.split(window.npGlobalProp.npPropSplitter);
    $.each(npMegaObj, function (isobj, myobj) {
        $.each(myobj, function (key, value) {
            if (validationStatus) {
                if (validateArr.indexOf(key) > -1) {
                    if (value == "0" || value == "" || value == null) {
                        $('#' + key + '-validation').append(key + " field must not empty.");
                        $('#' + key).focus();
                        //alertify.error(key + " field must not empty.");
                        validationStatus = false;
                    }
                }
            }
        });
    });
    return validationStatus;
};


var base_url = function () {
    console.log(location.pathname);
};

var np_submit = function (npFormControl, npValidate, url, messageType) {
    var isValid = true;
    var npMegaObj = npGetMegaObj(npFormControl);
    console.log(npMegaObj);
    if (npValidate.IsChecked) {
        isValid = npValidation(npMegaObj, npValidate);
    }

    if (!isValid) return false;

    return npSave(npMegaObj, url, messageType);
};


var file_submit = function (evt, url, model) {
    //Retrieve the first (and only!) File from the FileList object
    var file = $('#' + evt)[0].files[0];
    var pdata = {};
    var objName = model.ControlStr.split(npGlobalProp.npObjNameSplitter)[0];
    var propList = model.ControlStr.split(npGlobalProp.npObjNameSplitter)[1].split(npGlobalProp.npPropSplitter);
    if (file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var contents = e.target.result;
            var contArr = contents.split('\n');
            console.log('Inside file processing...');
            var testobj = [];
            $.each(contArr, function (key, value) {
                var strToArray = value.split('|');
                var temp = {};
                var j = 0;
                $.each(strToArray, function (k, v) {
                    var propName = propList[j++];
                    temp[propName] = v;
                });
                testobj.push(temp);
            });
            pdata[objName] = testobj;
            //console.log(pdata);
            npSave(pdata, url);
        };
        reader.readAsText(file);
    } else {
        alert("Failed to load file");
    }
};

// convert javascript datetime format(milisecond) to UTC format
/*
*   javascript fatch datetime in milisecond format.
*   this function convert milisecond to actual understandable datetime
*/
function ConvertMillisecondToDate(param) {
    //var result;
    if (param == '' || param == null) return param;
    if (!_.isString(param)) return param;
    if (param.indexOf("/Date") > -1) {
        var y = new Date(parseInt(param.substr(6)));
        return y.getDate() + '/' + (y.getMonth() + 1) + '/' + y.getFullYear();
    }
    return param;
}
/* 
 * this converter substract 13 hours from our local datetime.
 * created by Mohammed Sabbir
*/
function ConvertMillisecondToDateOnline(param) {
    var result;
    if (param.indexOf("/Date") > -1) {
        var y = new Date(parseInt(param.substr(6)) - (13 * 3600000));
        result = y.getDate() + '/' + (y.getMonth() + 1) + '/' + y.getFullYear();
    } else {
        result = "";
    }
    return result;
}

// convert null variable to widespace
function ConvertNullToEmpty(result) {
    var resultstring = JSON.stringify(result);
    resultstring = resultstring.split("null").join('" "');
    result = JSON.parse(resultstring);
    return result;
}
/*
*   convert boolean value to viewable yes/no string
*
*/
function booleantoString(a) {
    var result = "";
    if (a && a != " ") {
        result = "Yes";
    } else if (!a) {
        result = "No";
    } else {
        result = " ";
    }
    return result;
}

/*
*   use to show halt time.
*/
function loading() {
    $(".btn").isLoading();
    setTimeout(function () {
        // Deactivate the loading plugin
        $(".btn").isLoading("hide");
    }, 5000);
}

function FlashMessage(flashLabel, message) {
    if (flashLabel == 1) {
        $('.savebtn').attr('disabled', 'disable');
    }
    var $flash = $('<div id="flash" style="display:none;">');
    $flash.html(message);
    $flash.toggleClass('flash');
    $flash.toggleClass('flash-' + flashLabel);
    $('body').find('.FlashMessage').prepend($flash);
    $flash.slideDown('slow');
    $flash.delay(2000).slideToggle('highlight');
    $($flash).click(function () { $(this).slideToggle("highlight"); });
    if (flashLabel == 1) {
        setTimeout((function () {
            window.location.reload();
        }), 1500);
    }
}