﻿var test = function(control) {
    var x = $(control).find(':selected').attr('value');
    alert(x);
};

$(function () {
    var screenResolution = $(window).height();
    var a = (76 * screenResolution) / 100;
    $(".ERP-panel-body").css("max-height", a);

    $("#checkAll").on("change", function() {
        var container = $(this).parent().parent().next();
        if ($(this).is(":checked")) {
            $(container).find("input:checkbox").each(function() {
                $(this).prop('checked', true);
            });
        } else {
            $(container).find("input:checkbox").each(function() {
                $(this).prop('checked', false);
            });
        }
    });
    
});


(function ($) {

    $(document).on('hidden.bs.modal', '.modal', function () {
            $(document.body).removeClass('modal-noscrollbar');
        })
        .on('show.bs.modal', '.modal', function () {
            // Bootstrap adds margin-right: 15px to the body to account for a
            // scrollbar, but this causes a "shift" when the document isn't tall
            // enough to need a scrollbar; therefore, we disable the margin-right
            // when it isn't needed.
            if ($(window).height() >= $(document).height()) {
                $(document.body).addClass('modal-noscrollbar');
            }
        });

})(window.jQuery);