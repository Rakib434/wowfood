﻿
function SaveInvoice(url) {
    ShowLoader("Processing......Wait..it will take some time.");
    var propList = 'Quantity^BuyingPrice^SellingPrice^TotalPrice'.split('^');
    var InvoiceDetails = [];

    $('#productBody tr').each(function (i, element) {
        var productId = parseFloat($(element).find('input[id="product-id"]').val());
        var unitPrice = parseFloat($(element).find('td input[id="product-unit-price"]').val());
        var quantity = parseFloat($(element).find('td input[id="product-quantity"]').val());
        if (quantity < 1)
            return;
        var product = {};
        product.ProductId = productId;
        product.Quantity = quantity;
        product.SellingPrice = unitPrice;

        InvoiceDetails.push(product);
    });
    var invoiceInfo = {};
    //var z=
    invoiceInfo.Id = $('#invoiceInfo-id').val();
    invoiceInfo.InvoiceDate = $('#Invoice_date').val();
    invoiceInfo.CreatedDate = $('#created-date').val();
    invoiceInfo.InvoiceNo = $('#InvoiceNo').val();
    invoiceInfo.PropertyId = $('#property-id').val();
    console.log(invoiceInfo);
    console.log(InvoiceDetails);

    //======save Invoice========
    $.post(url, { param: invoiceInfo, InvoiceDetailsList: InvoiceDetails }, function (data) {
        if (data.MessageType === 1)
            alertify.success(data.MessageData);
        else
            alertify.alert(data.MessageData);
        console.log(data);
        HideLoader();
    });
}
//==================

function ShowLoader(message) {
    var loader = document.getElementById("overlay");
    loader.innerHTML = '<div class="msg-loading">' +
        message +
        '</div>';
    loader.style.display = "block";
}
function HideLoader() {
    document.getElementById("overlay").style.display = "none";
}