﻿//============
var pressNumber = 0;
var pressdecimal = 1;
var subTotalglobal = 0;
var givenGlobal = 0;
var returnGlobal = 0;
var products = []

function addata(event, inputObject) {
    if (event.keyCode === 13) {
        var proid = document.getElementById('pID_' + inputObject.id);
       // console.log("This Is Product Row ID");
       // console.log(proid.innerHTML);
        var price = document.getElementById('price_' + inputObject.id).value;
        var SubTotal = (inputObject.value * price);

        var proname = document.getElementById('pName_' + inputObject.id).innerHTML;
        var table = document.getElementById("productTable");

        //ajax call product stock

        var product = { id: inputObject.id, pname: proname, pId: proid.innerHTML, price: price, quantity: inputObject.value };
        getProductStock(proid.innerHTML, product);

        //console.table(products);
    }
}

function getProductStock(productid, productObj) {
    var url = 'GetCurrentStockForSales';
    $.ajax({
        data: { productID: productid },
        type: 'POST',
        cache: false,
        url: url,
        success: function (result) {
            if (result < 5 && result > 0) {
                alert("Low Qunatity");
            }
            else if (result == 0) {
                alert("Stock Is Empty For This Product.");
                return;
            }
            //else if (result>0)
            //{

            //}
            productObj.stock = result
            updateProductArrayAndTable(productObj);
        },
        error: function (error) {
        }
    });
}

function updateProductArrayAndTable(product) {
    //find item in products
    var productObject = $.grep(products, function (item) {
        return item.id === product.id;
    })[0];

    //console.log(products[quantity]);
    //find the product in the array
    if (productObject) {
        var index = products.indexOf(productObject);
        //delete the item
        if (Number(product.quantity) < 1) {
            if (index > -1)
                products.splice(index, 1);
        }
        else {
            //Update the item 
            if (index > -1)
                products[index] = product;
        }
    }
    else
        products.push(product);
    updateTable();
    SumTotal();
}

function updateTable() {
    $("#productTable > tbody").html("");
    products.forEach(function (item) {
        if (item.quantity < 1)
            return;
        else if (item.quantity > item.stock)
        {
            alert("Quantity Is Larger Than Stock.");
            return;
        }
        //$('#productTable > tbody:last-child').append('<tr><td>' + item.pId + '</td><td>' + item.pname + '</td><td>' + item.price + '</td><td>' + item.quantity + '</td><td class="stock">' + (item.stock - item.quantity) + '</td><td class="count-me">' + item.quantity * item.price + '</td></tr>');
        $('#productTable > tbody:last-child').append('<tr><td>' + item.pname + '</td><td>' + item.price + '</td><td>' + item.quantity + '</td><td class="stock">' + (item.stock - item.quantity) + '</td><td class="count-me">' + item.quantity * item.price + '</td></tr>');
    });
}
//Move With ALT
$(document).bind("keydown", function (e) {
    if (e.altKey) {
        console.log("Alter key");

        var s = document.getElementById("printsave").disabled;
        var p = document.getElementById("justprint").disabled;
        var r = document.getElementById("refresh").disabled;
        //console.log(s);
        //console.log(r);
        //console.log(p);

        if (e.keyCode >= 48 && e.keyCode <= 58) {
            //console.log("Other  key" + e.keyCode);
            pressNumber = (pressNumber * pressdecimal) + (e.keyCode - 48);
            pressdecimal = pressdecimal * 10;
            var elem = document.getElementById(pressNumber);
            document.getElementById(pressNumber).focus();
            console.log("alt Press " + pressNumber);
        }
                
        else if (e.keyCode === 67) {
            document.getElementById("givenAmount").focus();
        }
        else if (e.keyCode === 82) {
            refresh();  // Print and Save 80=R
        }
        else if (e.keyCode === 80) {
            justPrint();    // Print 80=P
        }
        else if (e.keyCode === 83) {
            printDiv('printableArea'); // Print and Save 83=S
        }
    } else {
        //console.log(e);
        pressNumber = 0;
        pressdecimal = 1;
    }
});

function increase(inputObject) {

    var proid = document.getElementById('btn_' + inputObject.id).id;

    $.each(products, function (index, value) {
        //console.log(value.id);
        if (value.id === proid) {
            var qty = parseInt(value.quantity);
            value.quantity = qty + 1;
            //console.log(value.quantity);
            updateTable();
            //console.table(products);
        }
        else {
            // Add This Product Details To Table Array products[], adn update table.
        }

    });
}

function decrease(inputObject) {

    var proid = document.getElementById(inputObject.id).id;

    $.each(products, function (index, value) {
        //console.log(value.id);
        if (value.id === proid) {
            var qty = parseInt(value.quantity);
            if (qty > 0) {
                value.quantity = qty - 1;
                //console.log(value.quantity);
                updateTable();
            }

        }

    });
}

function SaveInvoice() {

    var invoiceDetailList = [];
    //var invoiceDetail = {};
    //console.log(products);
    products.forEach(function (element) {
        var invoiceDetail = {};
        invoiceDetail.ProductId = element.pId;
        invoiceDetail.ProductName = element.pname;
        invoiceDetail.SellingPrice = element.price;
        invoiceDetail.Quantity = element.quantity;
        invoiceDetail.TotalPrice = element.quantity * element.price;
        invoiceDetailList.push(invoiceDetail);

    });

    var invoiceInfo = {};

    invoiceInfo.SubTotal = subTotalglobal;
    invoiceInfo.ReturnTotal = returnGlobal;
    invoiceInfo.InvoiceNo = invoiceno;

    var url = 'AddSalesInvoice';
    $.ajax({
        data: { param: invoiceInfo, InvoiceDetailsList: invoiceDetailList },
        type: 'POST',
        cache: false,
        url: url,
        success: function (result) {
            //if (result.MessageType === 1) {
            //    setTimeout(function () {
            //        location.reload();
            //        //your code to be executed after 1 second
            //    }, 1500);
            //}
            
        },
        error: function (error) {
        }
    });
}

document.getElementById("justprint").disabled = true;
document.getElementById("refresh").disabled = true;

function printDiv(printableArea) {

    var printContents = document.getElementById(printableArea).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
    SaveInvoice();

    //--------token number update---------
    //var invoiceNo = parseInt(invoiceno) + 1;
    //invoiceno = invoiceNo + '';
    //console.log(@ViewBag.username);

    // Disable Enable Button
    document.getElementById("printsave").disabled = true;
    document.getElementById("justprint").disabled = false;
    document.getElementById("refresh").disabled = false;
}

function justPrint(printableArea) {

    var printContents = document.getElementById(printableArea).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;

}

//function justSave() {
//    SaveInvoice();
//    //--------token number update---------
//    var invoiceNo = parseInt(invoiceno) + 1;
//    invoiceno = invoiceNo + '';
//    //console.log(invoiceNo);
//}

function refresh()
{
    location.reload();
    //--------token number update---------
    var invoiceNo = parseInt(invoiceno) + 1;
    invoiceno = invoiceNo + '';
}

function SumTotal() {
    var tds = document.getElementById('productTable').getElementsByTagName('td');
    var sum = 0;
    for (var i = 0; i < tds.length; i++) {
        if (tds[i].className === 'count-me') {
            sum += isNaN(tds[i].innerHTML) ? 0 : parseInt(tds[i].innerHTML);
        }
    }
    subTotalglobal = sum;

    document.getElementById('totalAmount').innerHTML = sum + " BDT";



}

function remainingAmount(e) {
    if (e.keyCode === 13) {
        var tds = document.getElementById('givenAmount').value;
       // console.log(tds);
        //var sCharge = document.getElementById('serviceCharge').value;
        var tBill = document.getElementById('totalAmount').innerHTML;
        var newsum = parseInt(tds) - parseInt(tBill);

        //subTotalglobal = tBill;
        //serviceGlobal = sCharge;
        //console.log(newsum);
        givenGlobal = tds;
        returnGlobal = newsum;

        document.getElementById('returnAmount').innerHTML = newsum + " BDT";
        document.getElementById('givenAmountdiv').innerHTML = givenGlobal + " BDT";
    }
}

