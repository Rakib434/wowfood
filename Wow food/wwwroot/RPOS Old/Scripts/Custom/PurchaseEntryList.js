﻿

        $(function() {
            $('.dataTables').dataTable({ "aaSorting": [] });
        });

        $(document).ready(function () {

            $(document).on('focus', '.datefrom', function () {
                $('.datefrom').datepicker({ autoclose: true, dateFormat: "dd/MM/yy", maxDate: 0 }).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                }).change(function () {
                    if ($('.dateto').val() != null && $('.dateto').val() !== "") {
                        var datefrom = new Date($('.datefrom').val());
                        var dateto = new Date($('.dateto').val());
                        if (datefrom > dateto) {
                            $('.dateto').val("");
                        }
                    }
                });
                //.datepicker("setDate", "0");
            });
            $(document).on('focus', '.dateto', function () {
                $('.dateto').datepicker({ autoclose: true, dateFormat: "dd/MM/yy", maxDate: 0 }).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                }).change(function () {
                    if ($('.datefrom').val() != null && $('.datefrom').val() !== "") {
                        var datefrom = new Date($('.datefrom').val());
                        var dateto = new Date($('.dateto').val());
                        if (datefrom > dateto) {
                            $('.dateto').val("");
                        }
                    }
                });
                //.datepicker("setDate", "0");

            });
        });
    