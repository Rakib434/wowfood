﻿using System;
using System.Data;
using System.Globalization;
using System.Security;
using System.Security.Permissions;
using System.Web;
using ERP.INFRASTRUCTURE.ReportEntity;
using Microsoft.Reporting.WebForms;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.ReportRepositories;
using System.Collections;

namespace ERP.WEB.Report
{
    public partial class ReportForm : System.Web.UI.Page
    {
        readonly ReportPortal _reportPortal = new ReportPortal();
        //readonly ReportPortal _reportPortal = new ReportPortal(new UserSetupManager(), new VendorManager(), new ProjectManager(), new ItemManager(), new CategoryManager());
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["ReportID"] != null)
                    {

                        var reportSearchData = Request.QueryString["ReportID"].ToString(CultureInfo.InvariantCulture);
                        var splitdata = reportSearchData.Split('^')[0];
                        var splitobj = reportSearchData.Split('^')[1];
                        const string currentUrl = "Report/";
                        //string currentURL = "Report/BAPSA/";
                        switch (splitdata)
                        {
                           
                            
                            case "EmployeePrint":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "PaySlip.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PaySlipDS", GetEmployeeById(splitobj)));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;

                            case "SalarySheet":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "SalarySheet.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("SalarySheetDS", GetSalarySheet()));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "InvoiceReport":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "InvoiceReport.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("InvoiceReportDtSet", GetInvoiceReportData(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("HederDtSet", GetHeaderDataDateFirst(splitobj, "Invoice Report")));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "StockListReport":

                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "StockReport.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("StockReportDtSet", GetStockListReportData(splitobj)));
                                //RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("HederDtSet", GetHeaderDataDateFirst(splitobj, "Invoice Report")));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "OpeningStock":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "StockReport.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("StockReportDtSet", GetOpeningStocktData(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("HederDtSet", GetHeaderData(splitobj, "Opening Stock")));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "ClosingStock":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "StockReport.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("StockReportDtSet", GetClosingStockData(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("HederDtSet", GetHeaderData(splitobj, "Purchase Report")));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "InvoiceList":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "InvoicePrint.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("InvoicePrintDtSet", GetInvoiceById(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("InvoicePrintDetalDtSet", GetInvoiceDetaileById(splitobj)));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "challan":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "ChallanPrint.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ChallanPrintDtSet", GetInvoiceById(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ChallanDetailDtSet", GetChallanDetaileById(splitobj)));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "InvoiceShortPrint":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "InvoiceShortPrint.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("InvoiceShortDtSet", GetInvoiceById(splitobj)));
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("InvoiceShortDetailDtSet", GetChallanDetaileById(splitobj)));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                            case "VoucherPrint":
                                RdlcReportViewer.Reset();
                                RdlcReportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                                RdlcReportViewer.ProcessingMode = ProcessingMode.Local;
                                RdlcReportViewer.LocalReport.ReportPath = currentUrl + "DebitVoucher.rdlc";
                                RdlcReportViewer.LocalReport.DataSources.Clear();
                                RdlcReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DebitVoucherDS", GetVoucher(splitobj)));
                                RdlcReportViewer.DataBind();
                                RdlcReportViewer.LocalReport.Refresh();
                                break;
                        }
                    }
                }
            }
            catch (HttpException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
        }


        private IEnumerable GetVoucher(string id)
        {
            var result = _reportPortal.GetVoucher(id);
            return result;
        }

        private IEnumerable GetEmployeeById(string employeeId)
        {
            var result = _reportPortal.GetEmployeeById(employeeId);
            return result;
        }
        private IEnumerable GetSalarySheet()
        {
            var result = _reportPortal.GetSalarySheet();
            return result;
        }


        private IEnumerable GetStockListReportData(string splitobj)
        {
            return _reportPortal.GetStockListData(splitobj, "");
        }
        private IEnumerable GetInvoiceDetaileById(string splitobj)
        {
            var result = _reportPortal.GetInvoiceDetailById(splitobj);
            return result;
        }
        private IEnumerable GetChallanDetaileById(string splitobj)
        {
            var result = _reportPortal.GetChallanDetaileById(splitobj);
            return result;
        }
        private IEnumerable GetInvoiceById(string splitobj)
        {
            var result = _reportPortal.GetInvoiceById(splitobj);
            return result;
        }



        private IEnumerable GetClosingStockData(string splitobj)
        {
            return _reportPortal.GetClosingStockData(splitobj);
        }

        private IEnumerable GetOpeningStocktData(string splitobj)
        {
            return _reportPortal.GetOpeningStocktData(splitobj);
        }

        
        private IEnumerable GetInvoiceReportData(string splitobj)
        {
            return _reportPortal.GetInvoiceReportData(splitobj);
        }

        private IEnumerable GetHeaderData(string splitobj,string reportName)
        {
            var spltedObjects = splitobj.Split('_');
            var endDateString = spltedObjects.Length>1? spltedObjects[spltedObjects.Length - 2]:"";
            var startDateString = spltedObjects.Length>1?spltedObjects[spltedObjects.Length - 3]:"";

            var endDate =endDateString==""?DateTime.Today: Convert.ToDateTime(endDateString);
            var satrtDate = startDateString == "" ? DateTime.Today : Convert.ToDateTime(startDateString);
            IEnumerable obj = new[] { new { FromDate = satrtDate.ToString("dd-MM-yyyy"), ToDate = endDate.ToString("dd-MM-yyyy"), ReportName = reportName, CompanyName = "", Remarks = "" } };
            return obj;
            ;
        }
      
        private IEnumerable GetHeaderDataDateFirst(string splitobj, string reportName)
        {
            var spltedObjects = splitobj.Split('_');
            var endDateString = spltedObjects.Length > 1 ? spltedObjects[0] : "";
            var startDateString = spltedObjects.Length > 1 ? spltedObjects[1] : "";

            var endDate = endDateString == "" ? DateTime.Today : Convert.ToDateTime(endDateString);
            var satrtDate = startDateString == "" ? DateTime.Today : Convert.ToDateTime(startDateString);
            IEnumerable obj = new[] { new { FromDate = satrtDate.ToString("dd-MM-yyyy"), ToDate = endDate.ToString("dd-MM-yyyy"), ReportName = reportName, CompanyName = "", Remarks = "" } };
            return obj;
            ;
        }


    }
}